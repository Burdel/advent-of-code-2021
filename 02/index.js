const fs = require('fs');

fs.readFile('input', 'utf8', (err, value) => {
    const lineValues = value.split('\r\n');
    console.log(`Part 1: Horizontal * Depth = ${calculatePosition(lineValues)}`);
    console.log(`Part 2: Horizontal * Depth with Aim = ${calculatePositionWithAim(lineValues)}`);
});

function calculatePosition(array) {
    let horizontal = 0;
    let depth = 0;

    for (let i = 0; i < array.length; i++) {
        const [ command, valueString ] = array[i].split(' ');
        const value = parseInt(valueString);
        switch (command) {
            case 'forward':
                horizontal += value;
                break;
            case 'up':
                depth -= value;
                break;
            case 'down':
                depth += value;
                break;
        }
    }
    return horizontal * depth;
}

function calculatePositionWithAim(array) {
    let horizontal = 0;
    let depth = 0;
    let aim = 0;

    for (let i = 0; i < array.length; i++) {
        const [ command, valueString ] = array[i].split(' ');
        const value = parseInt(valueString);
        switch (command) {
            case 'down':
                aim += value;
                break;
            case 'up':
                aim -= value;
                break;
            case 'forward':
                horizontal += value;
                depth += (aim * value);
                break;
        }
    }
    return horizontal * depth;
}
