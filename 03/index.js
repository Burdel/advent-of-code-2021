const fs = require('fs');

const lines = fs.readFileSync('input', 'utf8').split('\r\n').filter(line => line !== '');

console.log(`Part 1: Gamma * Epsilon = ${part1(lines)}`);
console.log(`Part 2: Oxygen Generator Rating * CO2 Scrubber Rating = ${part2(lines)}`);

function part1(array) {
    let gamma = getMostCommonBits(array);
    let epsilon = invertBinaryString(gamma);
    return parseInt(gamma, 2) * parseInt(epsilon, 2);
}

function part2(array) {
    let oxygenGeneratorRating = reduceByCommonBits(array, true);
    let co2ScrubberRating = reduceByCommonBits(array, false);
    return parseInt(oxygenGeneratorRating, 2) * parseInt(co2ScrubberRating, 2);
}

function getMostCommonBits(array) {
    let result = '';
    for (let i = 0; i < array[0].length; i++) {
        result += getMostCommonBitAtIndex(array, i);
    }
    return result;
}

function getMostCommonBitAtIndex(array, index) {
    let counter = 0;
    for (let j = 0; j < array.length; j++) {
        if (parseInt(array[j][index]) === 1) {
            counter++;
        }
    }
    return (counter >= array.length / 2) ? '1' : '0';
}

function invertBinaryString(binaryAsString) {
    let binary = parseInt(binaryAsString, 2);
    return (~binary >>> 0).toString(2).slice(-binaryAsString.length)
}

function reduceByCommonBits(array, mostCommon) {
    let index = 0;
    let mask = '';
    while (array.length > 1) {
        let mostCommonBit = getMostCommonBitAtIndex(array, index);
        if (!mostCommon) {
            mostCommonBit = invertBinaryString(mostCommonBit);
        }
        mask += mostCommonBit;
        array = array.filter(value => value.slice(0, mask.length) === mask);
        index++;
    }
    return array;
}


