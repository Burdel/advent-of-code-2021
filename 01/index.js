const fs = require('fs');

fs.readFile('input', 'utf8', (err, value) => {
    const lineValues = value.split('\r\n');
    console.log(`Part 1: ${calcChanges(lineValues)} measurements increased`);
    console.log(`Part 2: ${calcChanges(generateGroups(lineValues))} measurements increased`);
});

function generateGroups(array) {
    const result = [];
    for (let i = 2; i < array.length; i++) {
        result.push(parseInt(array[i - 2]) + parseInt(array[i - 1]) + parseInt(array[i]));
    }
    return result;
}

function calcChanges(array) {
    let increased = 0;
    for (let i = 1; i < array.length; i++) {
        if (parseInt(array[i]) > parseInt(array[i - 1])) {
            increased++;
        }
    }
    return increased;
}
