const fs = require('fs');

const lines = fs.readFileSync('input', 'utf8').split('\r\n\r\n');
part1(lines);
part2(lines);

function part1(array) {
    const drawnNumbers = array[0].split(',');
    let boards = parseBingoBoards(array.slice(1));
    for (let drawnNumberIndex = 0; drawnNumberIndex < drawnNumbers.length; drawnNumberIndex++) {
        const drawnNumber = parseInt(drawnNumbers[drawnNumberIndex]);
        markHit(drawnNumber, boards);
        // first possible win is after round 5
        if (drawnNumberIndex >= 4) {
            const winners = evaluateWinners(boards);
            if (winners.length > 0) {
                winners.forEach(winner => {
                    console.log(`Part 1: Bingo on board ${winner.board} with number ${drawnNumber} with a total score of ${winner.score * drawnNumber} points!`);
                });
                return;
            }
        }
    }
}

function part2(array) {
    const drawnNumbers = array[0].split(',');
    let boards = parseBingoBoards(array.slice(1));
    let lastWinningBoard;
    for (let drawnNumberIndex = 0; drawnNumberIndex < drawnNumbers.length; drawnNumberIndex++) {
        const drawnNumber = parseInt(drawnNumbers[drawnNumberIndex]);
        markHit(drawnNumber, boards);
        // first possible win is after round 5
        if (drawnNumberIndex >= 4) {
            const winners = evaluateWinners(boards);
            if (winners.length > 0) {
                for (let winnerIndex = 0; winnerIndex < winners.length; winnerIndex++) {
                    const winningBoard = winners[winnerIndex];
                    lastWinningBoard = winningBoard;
                    lastWinningBoard.totalScore = winningBoard.score * drawnNumber;
                    boards.splice(winningBoard.board, 1);
                    for (let i = winnerIndex + 1; i < winners.length; i++) {
                        winners[i].board--;
                    }
                }
            }
        }
    }
    console.log(`Part 2: The last winning board wins with a total score of ${lastWinningBoard.totalScore} points!`);
}

function parseBingoBoards(array) {
    let result = [];
    array.forEach(boardString => {
        const board = [];
        const rowStrings = boardString.split('\r\n');
        rowStrings.forEach(rowString => {
            const row = [];
            const values = rowString.split(/\s+/).filter(value => value !== '');
            values.forEach(value => {
                row.push({
                    value: parseInt(value),
                    hit: false
                });
            });
            board.push(row);
        });
        result.push(board);
    });
    return result;
}

function markHit(hit, boards) {
    for (let boardIndex = 0; boardIndex < boards.length; boardIndex++) {
        for (let rowIndex = 0; rowIndex < boards[boardIndex].length; rowIndex++) {
            for (let columnIndex = 0; columnIndex < boards[boardIndex][rowIndex].length; columnIndex++) {
                if (boards[boardIndex][rowIndex][columnIndex].value === hit) {
                    boards[boardIndex][rowIndex][columnIndex].hit = true;
                }
            }
        }
    }
}

function evaluateWinners(boards) {
    let winners = [];
    for (let boardIndex = 0; boardIndex < boards.length; boardIndex++) {
        let score = 0;
        let rows = [ [], [], [], [], [] ];
        let columns = [ [], [], [], [], [] ];
        boards[boardIndex].forEach((row, rowIndex) => {
            row.forEach((value, columnIndex) => {
                rows[rowIndex].push(value.hit);
                columns[columnIndex].push(value.hit);
                if (!value.hit) {
                    score += value.value;
                }
            });
        });
        let bingo = false;
        rows.forEach(row => {
            if (!row.includes(false)) {
                bingo = true;
            }
        });
        columns.forEach(column => {
            if (!column.includes(false)) {
                bingo = true;
            }
        });
        if (bingo) {
            winners.push({
                board: boardIndex,
                score: score
            });
        }
    }
    return winners;
}
